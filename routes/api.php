<?php 

    //es crida l'enrutador
    $app = new hermes();

    //lloc de rutes


    $app->post('/user/save','UserController@save');
    $app->post('/auth/login','UserController@login');
    $app->post('/auth/isLogin','UserController@isLogin');

    //permisos
    if(middleware::auth()) {
        if(middleware::hasPermission('Read','users')) {
            $app->post('/user/dani','UserController@index');
        } else {
            error(401, 'Acceso no autorizado');
        }
    } else {
        $app->get('/inici','UserController@initial');//<-Fer que torni algun manual
    }
?>