<?php
    require_once 'route.php';

    class hermes {
        private $routes;

        function __construct() {
            $routes = new Route(true);
            $this->routes = $routes->getRoutes();
        }

        //F I L T R O   D E   M E T O D O S
        //GET ->
        public function get($url,$process) {
            $data = json_decode(file_get_contents('php://input'));
            if($_SERVER['REQUEST_METHOD']=='GET') {
                $route = explode('/',$url);
                for($i=0;$i<count($route);$i++) {
                    if($route[$i]=='{id}') {
                        $id=$this->routes[$i];
                        $can=$this->routes;
                        $can[$i]='{id}';
                        if($route==$can) {
                            $function = explode('@',$process);
                            $request = array('id'=>$id);
                            call_user_func($function,$data);
                            exit();
                        }
                        break;
                    }
                }
                if($route == $this->routes) {
                    $function = explode('@',$process);
                    $request = array();
                    foreach($_GET as $key=>$value) {
                        $ent = array($key=>$value);
                        $request += $ent;
                    }
                    call_user_func($function,$data);
                    exit();
                }
            }
        }
        //POST ->
        public function post($url,$process) {
            $data = json_decode(file_get_contents('php://input'));
            if($_SERVER['REQUEST_METHOD']=='POST') {
                $route = explode('/',$url);
                for($i=0;$i<count($route);$i++) {
                    if($route[$i]=='{id}') {
                        $id=$this->routes[$i];
                        $can=$this->routes;
                        $can[$i]='{id}';
                        if($route==$can) {
                            $function = explode('@',$process);
                            $request = array('id'=>$id);
                            call_user_func($function,$data);
                            exit();
                        }
                        break;
                    }
                }
                if($route == $this->routes) {
                    $function = explode('@',$process);
                    $request = array();
                    foreach($_POST as $key=>$value) {
                        $ent = array($key=>$value);
                        $request += $ent;
                    }
                    call_user_func($function,$data);
                    exit();
                }
            }
        }

        //PUT ->
        public function put($url,$process) {
            $data = json_decode(file_get_contents('php://input'));
            if($_SERVER['REQUEST_METHOD']=='PUT') {
                parse_str(file_get_contents('php://input'),$_PUT);
                $route = explode('/',$url);
                for($i=0;$i<count($route);$i++) {
                    if($route[$i]=='{id}') {
                        $id=$this->routes[$i];
                        $can=$this->routes;
                        $can[$i]='{id}';
                        if($route==$can) {
                            $function = explode('@',$process);
                            $request = array('id'=>$id);
                            call_user_func($function,$data);
                            exit();
                        }
                        break;
                    }
                }
                if($route == $this->routes) {
                    $function = explode('@',$process);
                    $request = array();
                    foreach($_PUT as $key=>$value) {
                        $ent = array($key=>$value);
                        $request += $ent;
                    }
                    call_user_func($function,$data);
                    exit();
                }
            }
        }
        //DELETE ->
        public function delete($url,$process) {
            $data = json_decode(file_get_contents('php://input'));
            if($_SERVER['REQUEST_METHOD']=='DELETE') {
                $route = explode('/',$url);
                for($i=0;$i<count($route);$i++) {
                    if($route[$i]=='{id}') {
                        $id=$this->routes[$i];
                        $can=$this->routes;
                        $can[$i]='{id}';
                        if($route==$can) {
                            $function = explode('@',$process);
                            $request = array('id'=>$id);
                            call_user_func($function,$data);
                            exit();
                        }
                        break;
                    }
                }
            }
        }
    }
?>